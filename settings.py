import requests
import json
import copy
import os
import re
from pprint import pprint as pp
from itertools import combinations, chain, permutations
from multiprocessing.dummy import Pool
from collections import defaultdict
import networkx as nx

# You may change these settings:
START_URL = "https://en.wikipedia.org/wiki/Lyman-alpha_blob"
CRAWL_DEPTH = 2  # Setting more than 3 is not recommended
SCRAPER_THREADS = 10

# Change these settings if you know what you're doing
ALLOWED_DOMAIN = "en.wikipedia.org"
ROOT_DOMAIN = "https://{}".format(ALLOWED_DOMAIN)
ROOT_DIR = os.path.dirname(os.path.realpath(__file__)) + "/"
UTILS_DIR = ROOT_DIR + "utils/"
DATA_FILE = UTILS_DIR + "data.json"
