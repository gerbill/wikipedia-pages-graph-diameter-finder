from settings import *


class Scraper(object):
    graph = dict()
    urls_queue = set()
    urls_total = set()
    urls_crawled = set()

    def __init__(self):
        self.levels_crawled = 0
        self.urls_total = set()
        self.urls_crawled = set()
        self.run()
        Scraper.save_graph_to_file()
        print("Total urls found:", len(Scraper.urls_total))

    def run(self):
        while self.levels_crawled < CRAWL_DEPTH:
            self.levels_crawled += 1
            Scraper.urls_total.add(START_URL)
            Scraper.urls_queue = Scraper.urls_total - Scraper.urls_crawled
            if not Scraper.urls_queue:
                break
            pool = Pool(SCRAPER_THREADS)
            pool.map(Scraper.worker, range(SCRAPER_THREADS))
            print("Depth level crawled:", self.levels_crawled, "| Total levels:", CRAWL_DEPTH)

    @staticmethod
    def worker(worker_id):
        while Scraper.urls_queue:
            queue_len = len(Scraper.urls_queue)
            if queue_len % 100 == 0:
                print("urls in queue:", queue_len)
            try:
                url = Scraper.urls_queue.pop()
            except KeyError:
                break
            found_urls = Scraper.get_urls(url)
            Scraper.graph[url] = list(found_urls)
            Scraper.urls_total = Scraper.urls_total | found_urls
            Scraper.urls_crawled.add(url)

    @staticmethod
    def get_urls(url):
        page_source = rget(url).text
        page_source = page_source.split('id="mw-navigation"')[0].split('id="bodyContent"')[-1]
        urls = re.findall(r'<a href=\"(/wiki/[\w\S]*)\"', page_source)
        urls = {ROOT_DOMAIN + url for url in urls if ":" not in url and "Main_Page" not in url}
        return urls

    @staticmethod
    def save_graph_to_file():
        with open(DATA_FILE, "w") as f:
            f.write(json.dumps(Scraper.graph))


def rget(url, retries=10, timeout=10):
    for i in range(retries):
        try:
            r = requests.get(url, timeout=timeout)
            return r
        except Exception as exc:
            if i > round(retries/2):
                print(exc)
