from settings import *


class Graph(object):
    longest_paths = dict()
    first_only = set()
    last_only = set()
    first_last = defaultdict(set)

    def __init__(self):
        with open(DATA_FILE, "r") as f:
            self.graph_dict = json.loads(f.read())
        self.check_vertices()

    def vertices(self):
        return list(self.graph_dict.keys())

    def edges(self):
        return self.generate_edges()

    def create_nx_graph(self):
        G = nx.DiGraph()
        for edge in self.edges():
            G.add_edge(*edge)
        while True:
            try:
                G.remove_edge(*nx.find_cycle(G)[-1])
            except:
                break
        return G

    def diameter_v2(self):
        print("Creating graph...")
        G = self.create_nx_graph()
        longest_path = nx.dag_longest_path(G)
        print("Success!")
        print("Graph diameter:", len(longest_path))
        print("Longest path:", " > ".join(longest_path))

    def generate_edges(self):
        for vertex in self.graph_dict:
            for neighbour in self.graph_dict[vertex]:
                # if vertex in Graph.first_only and neighbour in Graph.last_only:
                #     continue
                if vertex == neighbour:
                    continue
                Graph.longest_paths[(vertex, neighbour)] = (vertex, neighbour)
                yield (vertex, neighbour)

    def check_vertices(self):
        first_v = set(self.vertices())
        last_v = set()
        for k, v in self.graph_dict.items():
            for item in v:
                last_v.add(item)
        Graph.first_only = first_v - last_v
        Graph.last_only = last_v - first_v

    def find_all_paths(self):
        paths = set(self.edges())
        for i in range(10):
            path_length = len(paths)
            temp_paths = copy.deepcopy(paths)
            for path in temp_paths:
                if len(path) > len(set(path)):
                    paths.remove(path)
            for path_pair in combinations(temp_paths, 2):
                print(path_pair)
                p1, p2 = path_pair
                if p1[-1] == p2[0]:
                    paths.remove(p1)
                    paths.remove(p2)
                    longer_path = tuple(chain(p1, p2[1:]))
                    if len(set(longer_path)) < len(longer_path):
                        continue
                    first_last = (longer_path[0], longer_path[-1])
                    if first_last not in Graph.longest_paths or len(Graph.longest_paths[first_last]) < len(longer_path):
                        if first_last in Graph.longest_paths and Graph.longest_paths[first_last] in paths:
                            paths.remove(Graph.longest_paths[first_last])
                        paths.add(longer_path)
                        Graph.longest_paths[first_last] = longer_path
            print(i, len(paths))
            if len(paths) == path_length:
                break
        paths = sorted(list(paths), key=len, reverse=True)
        return paths

    def find_all_paths_v2(self):
        paths = set(self.edges())
        work = True
        while work:
            path_length = len(paths)
            print(path_length)
            to_remove = set()
            for p1, p2 in combinations(paths, 2):
                if p1[-1] == p2[0]:
                    longer_path = tuple(chain(p1, p2[1:]))
                    longer_path_len = len(longer_path)
                    first_last_vertices = (longer_path[0], longer_path[-1])
                    if len(set(longer_path)) != len(longer_path):
                        continue
                    to_remove.add(p1)
                    to_remove.add(p2)
                    if first_last_vertices not in Graph.first_last[longer_path_len]:
                        paths.add(longer_path)
                        Graph.first_last[longer_path_len].add(first_last_vertices)
            print(len(paths))
            for path in to_remove:
                paths.remove(path)
            if len(paths) == path_length:
                work = False
            print(len(paths))
        paths = sorted(list(paths), key=len, reverse=True)
        return paths

    def diameter(self):
        paths = self.find_all_paths_v2()
        print("Graph diameter is:", len(paths[0]))
        print("Longest path:", " > ".join(paths[0]))
