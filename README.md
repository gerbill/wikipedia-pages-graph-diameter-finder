# Wikipedia Pages Graph Diameter Finder
## Requirements
python3.5+

## Installation
Create virtualenv and activate it
```bash
virtualenv venv
```
```bash
source venv/bin/activate
```
Install requirements
```bash
pip install -r requirements.txt
```

## Adjust settings
In settings.py file you may change the following:

 * START_URL - Change it to some other en.wikipedia.org article url if you wish.
 * CRAWL_DEPTH - how deep should the scraper crawl (2 or 3 levels deep. Setting higher than 3 is not recommended).
 * SCRAPER_THREADS - how many threads should the scraper launch. Use fewer threads if your internet connection isn't great and you see many HTTP errors while scraper is running. 

## Running script
While virtual environment is activated type:
```bash
python main.py
```
And hit Enter. Script will first crawl wikipedia. Once its done with crawling it will create a graph out of scraped URLs (may be a time consuming process if there are more than 20k urls collected)
and then output longest possible path and its length to the console.